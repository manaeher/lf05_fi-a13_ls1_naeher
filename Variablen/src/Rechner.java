import java.util.Scanner; // Import der Klasse Scanner
public class Rechner
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
 int zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
 int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis = zahl1 + zahl2;

 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
 myScanner.close();
 
 //Die Subtraktion der zahl1 und zahl2
 //wird der Variable differenz zugewiesen.
 int differenz = zahl1 - zahl2;
 
 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
 System.out.print(zahl1 + " - " + zahl2 + " = " + differenz);
 myScanner.close();
 
 //Das Produkt der zahl1 und zahl2
 //wird der Variable produkt zugewiesen
 int produkt = zahl1 * zahl2;
 
 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
 System.out.print(zahl1 + " * " + zahl2 + " = " + produkt);
 myScanner.close();
 
 //Die Division der zahl1 und zahl1
 //wird der Variable quotient zugewiesen
 int quotient = zahl1 / zahl2;
 
 System.out.print("\n\n\nErgebnis der Division lautet: ");
 System.out.print(zahl1 + " / " + zahl2 + " = " + quotient);
 myScanner.close();
 }
}