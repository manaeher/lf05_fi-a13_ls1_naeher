import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Dieses Porgramm gibt Ihnen ein Quadrat aus Sternchen aus.\n" + 
						   "Bitte geben Sie die gew�nschte Seitenl�nge an : ");
		int l = tastatur.nextInt();
		
		for(int i = 1; i <= l; i++) {
			if(i == 1 || i == l) {          //gibt die erste und letzte Zeile aus
				for(int x = 0; x < l; x++) {
					System.out.print("* ");
				}
				System.out.println(""); //geht zur zweiten Zeile
			}
			else {
				System.out.print("*");    //f�ngt Zeile mit * an
				for(int y = 0; y < 2 * l - 3; y++) {  //f�llt die Zeile mit Leerzeichen zwischen den *
					System.out.print(" ");
				}
				System.out.println("*"); //schlie�t die Zeile mit * ab und geht zur n�chsten
			}
		}
		tastatur.close();	

	}

}
