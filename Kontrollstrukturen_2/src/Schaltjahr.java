import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
				
		System.out.println("Bitte geben Sie eine Jahreszahl ein: ");
		int jahr = tastatur.nextInt();
		boolean schaltJahr = false;
		
		if(jahr % 4 == 0)
			schaltJahr = true;
		
		if(schaltJahr && jahr % 100 == 0 && jahr % 400 != 0)
			schaltJahr = false;
		
		if(schaltJahr && jahr % 400 == 0)
			schaltJahr = true;
		
		if(schaltJahr)
			System.out.println("Das Jahr " + jahr + " ist ein Schaltjahr");
		else
			System.out.println("Das Jahr " + jahr + " ist kein Schaltjahr");
		
		tastatur.close();
	}

}
