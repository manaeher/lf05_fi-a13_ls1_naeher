import java.util.Scanner;

public class Ohmsches_Gesetz {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double r; //Widerstand
		double i; //Stromst�rke 
		double u; //Spannung
		char eingabe;
		
		System.out.println("###################################\n"+
						   "# Dieses Programm berechnet Werte #\n"+
						   "#     mit dem Ohmschen Gesetz     #\n"+
						   "#             R=U/I               #\n"+
						   "###################################\n\n"+
						   "Bitte geben Sie den zu berechnenden Wert ein: \n"+
						   " 'R' f�r den Wiederstand\n"+
						   " 'U' f�r die Spannung\n"+
						   " 'I' f�r die Stromst�rke");
		eingabe = tastatur.next().charAt(0);
		
		switch(eingabe)
		{
			case 'R':
				System.out.println("Bitte geben Sie die Stromst�rke in Ampere an: ");
				i = tastatur.nextDouble();
				System.out.println("Bitte geben Sie die Spannung in Volt an: ");
				u = tastatur.nextDouble();
				r = u / i;
				System.out.println("Der Widerstand betr�gt: " + r + " Ohm");
				break;
			case 'U':
				System.out.println("Bitte geben Sie den Widerstand in Ohm an: ");
				r = tastatur.nextDouble();
				System.out.println("Bitte geben Sie die Stromst�rke in Ampere an: ");
				i = tastatur.nextDouble();
				u = r * i;
				System.out.println("Die Spannung betr�gt: " + u + " Volt");
				break;
			case 'I':
				System.out.println("Bitte geben Sie die Spannung in Volt an: ");
				u = tastatur.nextDouble();
				System.out.println("Bitte geben Sie den Widerstand in Ohm an: ");
				r = tastatur.nextDouble();
				i = u / r;
				System.out.println("Die Stromst�rke betr�gt: " + i + " Ampere");
				break;
			default:
				System.out.println("               Fehlerhafte Eingabe!\n" + 
								   "Bitte geben Sie R, I oder U in Gro�buchstaben ein.");
				break;
				
		}
		tastatur.close();
	}

}
