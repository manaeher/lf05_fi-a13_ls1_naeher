import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		
		System.out.println("$$$$$$$$$$$$$$$$$$$$\n" + 
					 	   "$ Millionenrechner $\n" +
					 	   "$$$$$$$$$$$$$$$$$$$$\n");
		
		boolean inBenutzung = true;
		
		do
		{
			Scanner tastatur = new Scanner(System.in);
			System.out.println("Bitte geben Sie an wie gro� Ihre Einlage sein soll: ");
			double einlage = tastatur.nextDouble();
		
			System.out.println("Bitte geben Sie den erwartetetn Zinssatz ein: ");
			double zinssatz = tastatur.nextDouble();
			
			int jahre = 0;
			while(einlage < 1000000) {
				einlage = einlage + einlage / 100 * zinssatz;
				jahre++;
			}
			System.out.println("Unter diesen Bedingungen dauert es " + jahre + " Jahre bis Sie Million�r sind.");
			
			System.out.println("Wollen Sie noch eine Berechnung machen?" +
							   "			Ja[j]	Nein[n]			   ");
			char neustart = tastatur.next().charAt(0);
			switch(neustart)
			{
				case 'j':
					inBenutzung = true;
					break;
				case 'n':
					inBenutzung = false;
					break;
				default:
					inBenutzung = false;
					System.out.println("Falscha Eingabe. F�hrt herunter");
					break;
			}
			
		}
		while(inBenutzung);

	}

}
