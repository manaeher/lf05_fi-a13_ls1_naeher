﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	boolean inBenutzung = false;
        do
        {
        	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        	double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            rueckgeldAusgeben(rückgabebetrag);
            
            Scanner knopf = new Scanner(System.in);
            System.out.println("Möchten Sie weitere Tickets kaufen?\n\n"+
            				   "    Ja (1)     Nein (beliebige Zahl)");
            int ausschalten = knopf.nextInt();
            
            if(ausschalten == 1)
            	inBenutzung = true;
            else
            	inBenutzung = false;
            
        }
        while(inBenutzung);
        
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
        				   "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }   
    
    public static double fahrkartenbestellungErfassen() {
	    Scanner tastatur = new Scanner(System.in);
	    String[] fahrkartenBezeichnung = new String[10];
	    fahrkartenBezeichnung[0] = " 1   Einzelfahrschein Berlin AB  ";
	    fahrkartenBezeichnung[1] = " 2   Einzelfahrschein Berlin BC  ";
	    fahrkartenBezeichnung[2] = " 3   Einzelfahrschein Berlin ABC  ";
	    fahrkartenBezeichnung[3] = " 4   Kurzstrecke  ";
	    fahrkartenBezeichnung[4] = " 5   Tageskarte Berlin AB  ";
	   	fahrkartenBezeichnung[5] = " 6   Tageskarte Berlin BC  ";
	   	fahrkartenBezeichnung[6] = " 7   Tageskarte Berlin ABC  ";
	   	fahrkartenBezeichnung[7] = " 8   Kleingruppen-Tageskarte Berlin AB  ";
	   	fahrkartenBezeichnung[8] = " 9   Kleingruppen-Tageskarte Berlin BC  ";
	   	fahrkartenBezeichnung[9] = "10   Kleingruppen-Tageskarte Berlin ABC  ";
	   							
	    double[] fahrkartenPreise = new double[10];				//Vorteile: -Preise können unabhängig oder alle gleichzeitig 
	    fahrkartenPreise[0] = 2.90;                             //			 verändert werden
	    fahrkartenPreise[1] = 3.30; 							//          -Ausgabe des Menüs kann in zwei Code-Zeilen 
	    fahrkartenPreise[2] = 3.60;								//			 erfolgen und muss bei Veränderungen/Erweiterungen
	    fahrkartenPreise[3] = 1.90;								//			 nicht angepasst werden
	    fahrkartenPreise[4] = 8.60;								//          -Im Quellcode übersichtlicher 
	    fahrkartenPreise[5] = 9.00;								//Nachteile:-etwas mehr Schreibarbeit 
	    fahrkartenPreise[6] = 9.60;								//          -bei Hinzufügen von Tickets muss auch 
	    fahrkartenPreise[7] = 23.50;							//           der 'Switch-Case', die Größe des Arrays
	    fahrkartenPreise[8] = 24.30;							//           und der Inhalt beider Arrays angepasst werden
	    fahrkartenPreise[9] = 24.90;
	    																		
	    //Welches Ticket
	    System.out.println("Fahrkartenbestellvorgang:\n"+
	    				   "=========================\n\n"+
	    				   "Wählen Sie ihre Wunschfahrkarte aus:\n\n");
	    for(int x = 0; x < fahrkartenBezeichnung.length; x++) {
	    	System.out.printf(fahrkartenBezeichnung[x] + "%.2f€\n", fahrkartenPreise[x]);
	    }
	    				   
        
	    boolean running = true; 
	    double ticketPreis;
        do   
        {     
            int ticketArt = tastatur.nextInt();
            ticketPreis = 1;
            switch(ticketArt)
            {
                case 1:
                    ticketPreis = fahrkartenPreise[0];
                    System.out.println("Ihre Wahl: " + ticketArt);
                    running = false;
                    break;
                case 2:
                    ticketPreis = fahrkartenPreise[1];
                    System.out.println("Ihre Wahl: " + ticketArt);
                    running = false;
                    break;
                case 3:
                    ticketPreis = fahrkartenPreise[2];
                    System.out.println("Ihre Wahl: " + ticketArt);
                    running = false;
                    break;
                case 4:
                    ticketPreis = fahrkartenPreise[3];
                    System.out.println("Ihre Wahl: " + ticketArt);
                    running = false;
                    break;
                case 5:
                    ticketPreis = fahrkartenPreise[4];
                    System.out.println("Ihre Wahl: " + ticketArt);
                    running = false;
                    break;
                case 6:
                    ticketPreis = fahrkartenPreise[5];
                    System.out.println("Ihre Wahl: " + ticketArt);
                    running = false;
                    break;
                case 7:
                	ticketPreis = fahrkartenPreise[6];
                	System.out.println("Ihre Wahl: " + ticketArt);
                	running = false;
                	break;
                case 8:
                	ticketPreis = fahrkartenPreise[7];
                	System.out.println("Ihre Wahl: " + ticketArt);
                	running = false;
                	break;
                case 9:
                	ticketPreis = fahrkartenPreise[8];
                	System.out.println("Ihre Wahl: " + ticketArt);
                	running = false;
                	break;
                case 10:
                	ticketPreis = fahrkartenPreise[9];
                	System.out.println("Ihre Wahl: " + ticketArt);
                	running = false;
                	break;
                default:
                    System.out.println(">>falsche Eingabe<<");
                    break;
            }
        }
        while(running); //läuft bis einer der cases eintritt

        //Frage nach der Ticketanzahl
        System.out.print("Anzahl der Tickets: ");
        byte ticketAnzahl = tastatur.nextByte();
	   
        return ticketPreis * ticketAnzahl;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	// Geldeinwurf
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f EURO ", + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	 // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrscheine werden ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		   } 
           catch (InterruptedException e) {
 			e.printStackTrace();
 		   }
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	// Rückgeldausgabe
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", + rückgabebetrag );
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }     
}