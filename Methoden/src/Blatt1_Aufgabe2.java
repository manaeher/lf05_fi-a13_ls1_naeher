
public class Blatt1_Aufgabe2 {
	public static void main(String[] args) {
		float x = 2.36f;
		float y = 7.87f;
		
		System.out.println(produkt(x, y));
	}
	
	public static float produkt(float a, float b) {
		return a * b;
	}
}
