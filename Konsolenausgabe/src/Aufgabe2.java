
public class Aufgabe2 {

	public static void main(String[] args) {
		String s = "*";
		// s kann als Argument in printf eingesetzt werden und wird dann wieder als "*" ausgegeben
		
		System.out.printf( "\n%7s\n", s);
		System.out.printf( "%6s%s%s\n", s, s, s);
		System.out.printf( "%5s%s%s%s%s\n", s, s, s, s, s);
		System.out.printf( "%4s%s%s%s%s%s%s\n", s, s, s, s, s, s, s );
		System.out.printf( "%3s%s%s%s%s%s%s%s%s\n", s, s, s, s, s, s, s, s, s );
		System.out.printf( "%2s%s%s%s%s%s%s%s%s%s%s\n", s, s, s, s, s, s, s, s, s, s, s);
		System.out.printf( "%1s%s%s%s%s%s%s%s%s%s%s%s%s\n", s, s, s, s, s, s, s, s, s, s, s, s, s );
		System.out.printf( "%6s%s%s\n", s, s, s);
		System.out.printf( "%6s%s%s\n", s, s, s);	
	}

}
